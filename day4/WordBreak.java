package java222practice.day4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordBreak {
    public static boolean wordBreak(String s, List<String> wordList){
        boolean[] dp = new boolean[s.length() + 1];
        Set<String> set = new HashSet<>();
        for(String word : wordList){
            set.add(word);
        }

        dp[0] = true;
        for(int i = 1; i <= s.length(); i++){
            for(int j = 0; j < i; j++){
                if (dp[j]&&set.contains(s.substring(j, i))){
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[s.length()];

    }

    public static void main(String[] args) {
        String s = "topjava";
        List<String> wordDict = Arrays.asList("top", "java");
        System.out.println(wordBreak(s, wordDict));
    }
}
