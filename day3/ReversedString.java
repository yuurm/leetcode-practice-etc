package java222practice.day3;

public class ReversedString {
    public static void reversedString(char[] s){
        int len = s.length;
        int left = 0;
        int right = s.length - 1;
        while(left < right){
            char tmp = s[left];
            s[left] = s[right];
            s[right] = tmp;
            left++;
            right--;
        }

    }

    public static void main(String[] args) {
        char[] arr = {'t','o','p','j','a','v','a'};
        reversedString(arr);
        System.out.println(arr);
    }
}
