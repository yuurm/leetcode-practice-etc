package java222practice.day6;

public class BestTimeToBuyAndSellStock {
    public static int maxProfit(int[] prices){
        if(prices.length == 0) return 0;

        int maxProfit = 0;
        int buyAt = prices[0];
        for(int i = 1; i < prices.length; i++){
            maxProfit = Math.max(maxProfit, prices[i] - buyAt);
            if (buyAt > prices[i]){
                buyAt = prices[i];
            }
        }
        return maxProfit;
    }

    public static void main(String[] args) {
        int[] prices = {7,6,4,3,1};
        System.out.println(maxProfit(prices));
    }
}
