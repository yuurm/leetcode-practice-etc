package java222practice.day6;

import java.util.ArrayList;
import java.util.List;

public class FindAllDuplicatesInAnArray {
    public static List<Integer> findDuplicates(int[]nums){
        List<Integer>list = new ArrayList<>();
        for(int i = 0; i < nums.length; i++){
            int n = Math.abs(nums[i]);
            if(nums[n - 1] < 0){
                list.add(n);
            } else {
                nums[n - 1] = -nums[n - 1];
            }
        }
        return list;
    }

    public static void main(String[] args) {
        System.out.println(findDuplicates(new int[]{4,3,2,7,8,2,3,1}));
    }
}
