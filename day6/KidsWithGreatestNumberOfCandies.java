package java222practice.day6;

import java.util.ArrayList;
import java.util.List;

public class KidsWithGreatestNumberOfCandies {
    public static List<Boolean> kidsWithGreatestNumberOfCandies(int[]candies, int extraCandies){
        List<Boolean> list = new ArrayList<>();
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < candies.length; i++){
            max = Math.max(max, candies[i]);
        }
        for (int i = 0; i < candies.length; i++){
            if (candies[i] + extraCandies >= max){
                list.add(true);
            } else {
                list.add(false);
            }
        }
        return list;
    }

    public static void main(String[] args) {
        int[] candies = {4,2,1,1,2};
        int extraCandies = 1;
        System.out.println(kidsWithGreatestNumberOfCandies(candies, extraCandies));
    }
}
