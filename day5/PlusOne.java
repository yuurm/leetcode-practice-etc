package java222practice.day5;

import java.util.Arrays;

public class PlusOne {
    public static int[] plusOne(int[] digits) {
        int curry = 1;
        for (int i = digits.length - 1; i >= 0 && curry == 1; i--) {
            int tmp = digits[i] + curry;
            if (tmp >= 10) {
                curry = 1;
                digits[i] = tmp % 10;
            } else {
                curry = 0;
                digits[i] = tmp;
            }
        }

        if (curry != 0) {
            int[] newDigits = new int[digits.length + 1];
            newDigits[0] = 1;
            for (int i = 1; i < newDigits.length; i++) {
                newDigits[i] = digits[i - 1];

            }
            return newDigits;
        }
        return digits;
    }


    public static void main(String[] args) {
        int []digits = {1,1,9};
        System.out.println(Arrays.toString(plusOne(digits)));
    }


}
