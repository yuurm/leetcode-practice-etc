package java222practice.day5;

import static java.lang.System.currentTimeMillis;

public class JumpGame {
    static int[] nums = {2,3,1,1,4};
    static int[] nums2 = {3,2,1,0,4};

    public static boolean canApproach(int []nums){
        for (int i = 1; i < nums.length; i++){
            boolean isEnd = false;
            for (int j = i - 1; j >= 0; j--){
                if (i - j <= nums[j])
                    isEnd = true;
            }
            if (isEnd == false)
                return false;
        }
        return true;
    }

    public static boolean canApproachOpt(int []nums){
        int reachable = 0;
        for (int i = 0; i < nums.length && i <= reachable; i++) {
            reachable = Math.max(reachable, nums[i] + i);
            if (reachable >= nums.length - 1)
                return true;
        }
        return false;

    }

    public static void main(String[] args) {
        long a = currentTimeMillis();
        System.out.println(canApproach(nums));
        System.out.println(canApproach(nums2));
        long b = currentTimeMillis();
        System.out.println(b-a);

        long a1 = currentTimeMillis();
        System.out.println(canApproachOpt(nums));
        System.out.println(canApproachOpt(nums2));
        long b1 = currentTimeMillis();
        System.out.println(b1-a1);
    }

}
