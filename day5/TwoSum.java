package java222practice.day5;

import java.util.Arrays;
import java.util.HashMap;

public class TwoSum {
    public static int[] twoSum(int[] nums, int target){ //target = nums[i] + complement
        HashMap<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++){
            int complement = target - nums[i];
            if (map.containsKey(complement)){
                return new int[] {map.get(complement), i};
            } else {
                map.put(nums[i], i);
            }
        }
        throw new IllegalArgumentException("two sum is not equal target");
     }

    public static void main(String[] args) {
        int []nums = {3,2,4};
        int target = 6;

        int []resultArray = twoSum(nums, target);

        System.out.println(Arrays.toString(resultArray));
    }
}
