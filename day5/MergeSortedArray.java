package java222practice.day5;

public class MergeSortedArray {
    public static void merge(int[] nums1,int m, int[] nums2, int n){
        m--;
        n--;
        for (int i = m + n + 1; i >= 0; i--){
            int a = m >= 0 ? nums1[m] :Integer.MIN_VALUE;
            int b = n >= 0 ? nums2[n] :Integer.MIN_VALUE;
            if (a > b) {
                nums1[i] = a;
                m--;
            } else {
                nums1[i] = b;
                n--;
            }
        }
    }

    public static void main(String[] args) {
        //int []array1 = {1,2,3,0,0,0};
        //int []array2 = {2,5,6};

        int []array1 = {1,2,3,0,0,0};
        int []array2 = {2,5,6};
        merge(array1, 3, array2, 3);
        for (int i = 0; i < array1.length; i++){
            System.out.print(array1[i] + " ");
        }
    }
}
