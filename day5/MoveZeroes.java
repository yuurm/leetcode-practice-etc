package java222practice.day5;

import java.util.Arrays;

public class MoveZeroes {
    public static void moveZeroes(int[]nums){
        int lastNonZeroFountAt = 0;
        for (int i = 0; i < nums.length; i++){
            if(nums[i] != 0){
                nums[lastNonZeroFountAt] = nums[i];
                lastNonZeroFountAt++;
            }
        }
        for (int i = lastNonZeroFountAt; i < nums.length; i++){
            nums[i] = 0;
        }
    }

    public static void main(String[] args) {
        //int[]nums = {0,1,0,3,12};
        int[]nums = {0};
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
    }
}

//TODO:arrays
