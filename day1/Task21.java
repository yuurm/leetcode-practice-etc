package java222practice.day1;

public class Task21 {
    public static void main(String[] args) {
        boolean isInRange;
        boolean isValid;
        boolean isNotValid;
        boolean isEqual;

        int x = 8;

        isInRange = x > 0 & x < 5;
        System.out.println("& " + isInRange);

        isInRange = x > 0 && x < 5;
        System.out.println("&& " + isInRange);

        isValid = x > 0 | x > 5;
        System.out.println("| " + isValid);

        isValid = x > 0 || x > 5;
        System.out.println("|| " + isValid);

        isValid = (x != 8) ^ isInRange;
        System.out.println("^ " + isValid);

        isNotValid = !isValid;
        System.out.println(isNotValid);

        isEqual = isInRange == isValid;
        System.out.println(isEqual);

        boolean isNotEqual = isInRange != isValid;
        System.out.println(isNotEqual);


    }
}
