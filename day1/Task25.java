package java222practice.day1;

public class Task25 {
    public static void main(String[] args) {
        float f = 2.457f;
        int round = Math.round(f);
        System.out.println(round);

        double ceil = Math.ceil(3.4);
        System.out.println(ceil);

        double floor = Math.floor(3.9);
        System.out.println(floor);
    }
}
