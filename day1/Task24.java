package java222practice.day1;

public class Task24 {
    public static void main(String[] args) {
        double d = Math.random();
        System.out.println(d);

        //2 и 8
        double min = Math.min(2, 8);
        System.out.println(min);

        double max = Math.max(2, 8);
        System.out.println(max);

        int abs = Math.abs(-10);
        System.out.println(abs);

        double sqrt = Math.sqrt(4);
        System.out.println(sqrt);

        double pow = Math.pow(2,8);
        System.out.println(pow);

        double y = 3;
        double x = 4;
        double hypot = Math.hypot(x, y);
        System.out.println(hypot);
    }
}
