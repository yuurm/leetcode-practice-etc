package java222practice.day1;

public class Task20 {
    public static void main(String[] args) {
        int x1 = 5;
        int x2 = 5;
        int x3 = 3;
        int x4 = 7;

        boolean isEqual = (x1 == x2);
        System.out.println(isEqual);

        boolean isNonEqual = (x1 != x2);
        System.out.println(isNonEqual);

        boolean isGreater = (x1 >= x3);
        System.out.println(isGreater);

        //int t = x3==x4;

    }
}
