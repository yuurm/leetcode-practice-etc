package java222practice.day1;

public class Task11 {
    public static void main(String[] args) {
        int i0 = 10;
        System.out.println(i0);
        int i1 = 010;
        System.out.println(i1);
        int i2 = 0x10;
        System.out.println(i2);
        int i3 = 0b10;
        System.out.println(i3);

    }
}
