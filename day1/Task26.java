package java222practice.day1;

public class Task26 {
    public static void main(String[] args) {
        double pi = Math.PI;
        System.out.println(pi);
        System.out.println(Math.E);

        double rad180 = Math.toRadians(180);
        System.out.println(rad180);

        System.out.println(rad180 + " radians are " + Math.toDegrees(rad180) + " degrees");

        double cos180 = Math.cos(rad180);
        System.out.println("cos180 = " + cos180);

        double sin90 = Math.sin(Math.toRadians(90));
        System.out.println("sin90 = " + sin90);

        double atan = Math.atan(Math.toRadians(45));
        System.out.println("atan = " + atan);


        double atan2 = Math.atan2(rad180, rad180);
        System.out.println("atan2 = " + atan2);

        double log = Math.log(12);
        System.out.println("log = " + log);

        double log10 = Math.log10(12);
        System.out.println("log10 = " + log10);

    }
}
