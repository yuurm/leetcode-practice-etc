package java222practice.day1;

public class Task19 {
    public static void main(String[] args) {
        int o = 4 % 2;
        System.out.println(o);

        int o1 = 3 % 2;
        System.out.println(o1);

        int o2 = -3 % 2;
        System.out.println(o2);

        int o3 = 3 % -2;
        System.out.println(o3);

        int o4 = 5 % 10;
        System.out.println(o4);

        double y = 42.3;
        System.out.println(y % 10);

    }
}
