package java222practice.day1;

public class Task22 {
    public static void main(String[] args) {
        int x = 3;

        int t = x > 3 ? 1 : 0;
        System.out.println(t);

        t = (x < 3) ? x++ : --x;
        System.out.println(t);

        t = 10 + (x < 3 ? x++ : --x);
        System.out.println(t);

        t = 10 + (x < 3 ? (x > 10 ? x / 2 : x * 2) : --x);
        System.out.println(t);

    }
}
