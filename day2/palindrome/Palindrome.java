package java222practice.day2.palindrome;

public class Palindrome {
    public static boolean isPalindrome(int x){ //12_3_21
        if (x < 0 || (x % 10 == 0 && x != 0)){
            return false;
        }

        int revertedNumber = 0;

        while(x > revertedNumber){
            revertedNumber = revertedNumber*10 + x % 10;
            x = x / 10;
        }
        return x == revertedNumber || x == revertedNumber / 10;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(12321));
    }
}
