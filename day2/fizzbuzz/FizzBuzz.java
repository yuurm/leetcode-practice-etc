package java222practice.day2.fizzbuzz;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {
    public static List<String> fizzbuzz(int n) {
        List<String> res = new ArrayList<>();
        for (int i = 1; i <= n; i++){
            if (i % 3 == 0 && i % 5 == 0) {
                res.add("Fizzbuzz");
            }else if (i % 3 == 0){
                res.add("Fizz");
            } else if (i % 5 == 0){
                res.add("Buzz");
            } else {
                res.add((Integer.toString(i)));
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(fizzbuzz(15));
    }
}
