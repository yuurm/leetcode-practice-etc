package java222practice.day2.factorialendingzeros;

public class FactorialEndingZeros {
    public static int endingZeros(int n) {
        int count = 0;
        while (n > 4){
            n /= 5;
            System.out.println(n);
            count += n;
        }
        return n;
    }

    public static void main(String[] args) {
        System.out.println(endingZeros(25));
    }
}
