package java222practice.day2.hanoitowers;

import java.util.Stack;

public class HanoiTowers {
    private final int numDiscs;
    public final Stack<Integer> A = new Stack<>();
    public final Stack<Integer> B = new Stack<>();
    public final Stack<Integer> C = new Stack<>();


    public HanoiTowers(int numDiscs) {
        this.numDiscs = numDiscs;
        for (int i = 1; i <= numDiscs; i++){
            A.push(i);//LIFO
        }
    }

    private void move(Stack<Integer> begin, Stack <Integer> end, Stack<Integer> temp, int n){
        if (n == 1){
            end.push(begin.pop());
        } else {
            move(begin, temp, end, n - 1);
            move(begin, end, temp, 1);
            move(temp, end, begin, n - 1);
        }
    }

    public void solve (){
        move(A, B, C, numDiscs);
    }

    public static void main(String[] args) {
        HanoiTowers hanoiTowers = new HanoiTowers(3);

        hanoiTowers.solve();
        System.out.println(hanoiTowers.A);
        System.out.println(hanoiTowers.B);
        System.out.println(hanoiTowers.C);
    }
}
