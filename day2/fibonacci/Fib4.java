package java222practice.day2.fibonacci;

public class Fib4 {
    private static int fib4(int n){
        int last = 0;
        int next = 1;
        for (int i = 0; i < n; i++){
            int oldLast = last;
            last = next;
            next = oldLast + next;//2+3
        }
        return last;
    }

    public static void main(String[] args) {
        System.out.println(fib4(5));

    }
}


